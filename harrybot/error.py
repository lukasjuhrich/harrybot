# coding: utf-8

class ApiException(Exception):
    def __init__(self, msg, response):
        self.response = response
        super.__init__(msg)

# coding: utf-8
from base64 import b64decode, b64encode
from os import path

from json import JSONDecodeError
from requests import get, post, put

from .error import ApiException

URI = 'https://navigator.tu-dresden.de/api/'
ACCEPT_JSON = {'accept': 'application/json'}
CONTENT_TYPE_V1 = {'content-type': 'application/vnd.tud.navigator.v1+json'}


class Api:
    def __init__(self, path: str):
        self._path = path
        
    def _(self, next_path: str):
        whole_path = path.join(self._path, next_path)
        return type(self)(whole_path)
        
    def __getattr__(self, attr: str):
        return self._(attr)
        
    def __str__(self):
        return self._path
        
    def __repr__(self):
        return f"<Api {self._path}>"

    def get(self, *a, **kw):
        return get(self._path, *a, **kw)

    def post(self, *a, **kw):
        return post(self._path, *a, **kw)

    def put(self, *a, **kw):
        return put(self._path, *a, **kw)


def login_data(username, password):
    return {
        'username': b64encode(username.encode('utf-8')).decode('utf-8'),
        'password': b64encode(password.encode('utf-8')).decode('utf-8'),
        'university': 0,
    }


api = Api(URI)


def login(username: str, password: str) -> str:
    """Fetch Token from login"""
    response = api.login.post(
            json=login_data(username, password),
            headers={**CONTENT_TYPE_V1, **ACCEPT_JSON}
    )

    if not response:
        raise ApiException("Login went wrong", response)
    try:
        # expect: {'from', 'loginToken'}
        json = response.json()
        token = json['loginToken']
    except JSONDecodeError as e:
        raise ApiException("/login returned 200 but malformed body", response) from e
    except KeyError as e:
        raise ApiException("/login returned 200 but didn't provide 'loginToken' field",
                           response) from e 
    else:
        return token
        

def search(query: str, type=None):
    params = {'q': query}
    if type is not None:
        params.update(type=type.upper())
    response = api.search.get(params=params)
    if not response:
        raise ApiException("Search went wrong", response)
    try:
        # expect: {'query', 'results'}
        json = response.json()
        return json['results']
    except JSONDecodeError as e:
        raise ApiException("/search returned 200 but malformed body", response) from e
    except KeyError as e:
        raise ApiException("/search didn't provide the 'results' field", response) from e


def search_buildings(query: str):
    return search(query, type='building')
    
